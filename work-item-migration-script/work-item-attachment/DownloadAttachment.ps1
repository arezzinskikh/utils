# Trust all certificates
add-type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@
$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

# Function to download work item attachments with original file names
function DownloadWorkItemAttachments {
    param([string]$organization,[string]$srcCollection,[string]$srcProject,[string]$areaPath)

    try {
        $getWiUrl = "$organization$srcCollection$srcProject/_apis/wit/wiql?api-version=5.0"
        $body = @{
            "query"= "Select [System.Id] From WorkItems Where [System.AreaPath] Under '$areaPath'"
        }

        $wiResponse = Invoke-RestMethod -Uri $getWiUrl -Method POST -Body (ConvertTo-Json -InputObject $body) -ContentType "application/json;charset=UTF-8" -UseDefaultCredentials
        foreach ($workItem in $wiResponse.workItems) {
            $wiDetailsUrl = "$($workItem.url)?`$expand=all"
            $wiDetails = Invoke-RestMethod -Uri $wiDetailsUrl -Method Get -ContentType "application/json" -UseDefaultCredentials
            $attachments = ($wiDetails.relations | Where-Object { $_.rel -eq 'AttachedFile' })

            if ($attachments) {
                foreach ($attachment in $attachments) {
                    $fileName = $attachment.attributes.name
                    $folderPath = "C:\AdoProjects\log\attachments\$($areaPath -replace '\\', '-')\wi-$($wiDetails.id)"
                    New-Item -Path $folderPath -ItemType Directory -Force | Out-Null

                    $attachmentUrl = $attachment.url + "?filename=$fileName&download=true"
                    $localFilePath = "$folderPath\$fileName"
                    Invoke-WebRequest -Uri $attachmentUrl -OutFile $localFilePath -UseDefaultCredentials
                }
            }
        }
    }
    catch {
        Write-Host "Error: $($_.Exception.Message)"
        $_.Exception.Response | Out-Null
    }
}

# Main script to execute the retrieval and download
$srcOrganization = "http://moht-tfsado01:8080/tfs/"
$srcCollection = "DefaultCollection/"
$srcProject = "BI_EDW_SCRUM/"
$areaPath = "BI_EDW_SCRUM\תחום דאטה\BI-GIS\צוות 3"

DownloadWorkItemAttachments -organization $srcOrganization -srcCollection $srcCollection -srcProject $srcProject -areaPath $areaPath