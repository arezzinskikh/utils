Add-Type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@
$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

function CreateWorkItemFromJson {
    param([string]$jsonFilePath)

    $jsonData = Get-Content $jsonFilePath | ConvertFrom-Json

    $organizationUrl = "http://moht-tfsado01:8080/tfs"
    $collectionName = "MohCollection"
    $projectName = "MohMain"
    $apiVersion = "5.0"
    $createWiUrl = "$organizationUrl/$collectionName/$projectName/_apis/wit/workitems/`$$($jsonData.Type)`?api-version=$apiVersion"

    # Handling empty Assigned User field
    $assignedTo = if ($jsonData.AssignedUser) { $jsonData.AssignedUser } else { "unassigned" }

    $jsonBody = @(
        @{ op = "add"; path = "/fields/System.Title"; value = $jsonData.Title },
        @{ op = "add"; path = "/fields/System.AssignedTo"; value = $assignedTo },
        @{ op = "add"; path = "/fields/System.State"; value = $jsonData.State },
        @{ op = "add"; path = "/fields/System.Reason"; value = $jsonData.Reason },
        @{ op = "add"; path = "/fields/System.Description"; value = $jsonData.Description },
        @{ op = "add"; path = "/fields/Microsoft.VSTS.Common.AcceptanceCriteria"; value = $jsonData.AcceptanceCriteria },
        @{ op = "add"; path = "/fields/Microsoft.VSTS.Common.Discussion"; value = $jsonData.DiscussionDetails },
        @{ op = "add"; path = "/fields/System.AreaPath"; value = $jsonData.AreaPath },
        @{ op = "add"; path = "/fields/System.IterationPath"; value = $jsonData.IterationPath }
    ) | ConvertTo-Json -Depth 10

    try {
        $response = Invoke-RestMethod -Uri $createWiUrl -Method Post -ContentType "application/json-patch+json" -Body $jsonBody -UseDefaultCredentials
        Write-Host "Work item created successfully. ID: $($response.id)"
    } catch {
        Write-Host "Failed to create work item."
        if ($_.Exception.Response) {
            $reader = New-Object System.IO.StreamReader($_.Exception.Response.GetResponseStream())
            $responseBody = $reader.ReadToEnd()
            Write-Host "Response: $responseBody"
        }
    }
}

# Specify the path to your JSON file
$jsonFilePath = "C:\AdoProjects\log\payload.json"
CreateWorkItemFromJson -jsonFilePath $jsonFilePath