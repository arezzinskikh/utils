# Trust all certificates
add-type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@
$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

# Function to get all work items in a specified area path
function GetWorkItemsByAreaPath {
    param([string]$organization, [string]$collection, [string]$project, [string]$areaPath)
    try {
        $queryUrl = "$organization$collection$project/_apis/wit/wiql?api-version=5.1"
        $body = @{
            "query" = "Select [System.Id], [System.WorkItemType], [System.Title] From WorkItems Where [System.AreaPath] Under '$areaPath' And [System.WorkItemType] In ('Feature', 'Epic', 'Product Backlog Item', 'Task', 'Bug')"
        }
        $json = ConvertTo-Json -InputObject $body
        $response = Invoke-RestMethod -Uri $queryUrl -Method Post -Body $json -ContentType "application/json;charset=UTF-8" -UseDefaultCredentials
        return $response.workItems
    } catch {
        Write-Host "Error: " $_.Exception.Message
        return $null
    }
}

# Function to get detailed information of a work item by its URL
function GetWorkItemDetails {
    param([string]$url)
    try {
        $wiUrl=("{0}{1}" -f $url,"?`$`expand=all")
        $wiResponse=Invoke-RestMethod -Uri $wiUrl -Method Get -ContentType "application/json" -UseDefaultCredentials
        return $wiResponse
    } catch {
        Write-Host "Error retrieving work item details: " $_.Exception.Message
        return $null
    }
}

# Predefined dates for "2024 (DATA)"
$predefinedDates = @(
    "31.03-02.05.2024",
    "05.05-16.05.2024",
    "19.05-30.05.2024",
    "02.06-13.06.2024",
    "16.06-27.06.2024",
    "30.06-11.07.2024",
    "08.09-19.09.2024",
    "14.07-25.07.2024",
    "28.07-08.08.2024",
    "11.08-22.08.2024",
    "25.08-05.09.2024",
    "22.09-03.10.2024"
)

# Function to modify the System.IterationPath based on rules
function ModifyIterationPath {
    param([string]$iterationPath)

    # Handle null or empty IterationPath
    if (-not $iterationPath -or $iterationPath -eq "") {
        return "MohMain\\"
    }

    # Replace "BI_EDW_SCRUM" with "MohMain"
    $iterationPath = $iterationPath -replace "^BI_EDW_SCRUM", "MohMain"

    # Check if the path includes "2024"
    if ($iterationPath -match "MohMain\\2024\\(.+)$") {
        $subPath = $matches[1]

        # Check if the subPath matches a predefined date
        if ($predefinedDates -contains $subPath) {
            return "MohMain\\2024 (DATA)\\$subPath"
        } else {
            return "MohMain\\2024\\$subPath"
        }
    }

    # If not a 2024 path, return default
    return "MohMain\\"
}


# Function to get a field value or return a default
function GetFieldOrDefault {
    param(
        [PSCustomObject]$fields,
        [string]$fieldName,
        [string]$dataType = "string"
    )
    if ($fields.PSObject.Properties.Name -contains $fieldName) {
        $value = $fields.$fieldName
        if ($dataType -eq "integer") {
            if ($value -ne $null -and $value -eq 2) {
                return $value
            } else {
                return 2
            }
        }
        return $value
    } else {
        switch ($dataType) {
            'integer' { return 2 }
            'string'  { return "" }
            default   { return "" }
        }
    }
}

function CreateJsonFile {
    param([hashtable]$details, [string]$filePath)

    # Convert the hash table to readable (pretty-printed) JSON
    $jsonContent = $details | ConvertTo-Json -Depth 10

    # Fix backslashes in JSON for specific keys
    $jsonContent = $jsonContent -replace "\\\\\\\\", "\\"

    # Save the JSON to file with proper encoding
    Set-Content -Path $filePath -Value $jsonContent -Encoding UTF8
}

# Function to generate a JSON data object based on work item type
function GenerateDataObject {
    param([PSCustomObject]$detail, [string]$type)
    $fields = $detail.fields
    $assignedTo = GetFieldOrDefault -fields $fields -fieldName "System.AssignedTo"
    
    if ($assignedTo) {
        $assignedTo = $assignedTo.uniqueName
        if ($assignedTo -match '\\') {
            $assignedTo = $assignedTo
        } else {
            $assignedTo = "BRIUTNT\\$assignedTo"
        }
    } else {
        $assignedTo = ""
    }

    $workItemType = if ($type -eq "Initiative") { "Epic" } else { $type }
    $realState = GetFieldOrDefault -fields $fields -fieldName "System.State"

    $originalIterationPath = GetFieldOrDefault -fields $fields -fieldName "System.IterationPath"

    # Modify IterationPath for JSON output
    $modifiedIterationPath = try {
        ModifyIterationPath -iterationPath $originalIterationPath
    } catch {
        Write-Host "Error modifying IterationPath: $($_.Exception.Message)"
        "MohMain\\"  # Fallback
    }

    $baseData = @{
        "old-id" = $detail.id
        "ReferenceID" = "$($detail.id)-$workItemType"
        "ParentReferenceID" = if ($fields.'System.Parent') { $fields.'System.Parent' } else { "" }
        "System.Title" = GetFieldOrDefault -fields $fields -fieldName "System.Title"
        "System.AssignedTo" = $assignedTo
        "System.WorkItemType" = $workItemType
        "System.AreaPath" = "MohMain\DATA-management\Migration-test"
        "System.IterationPath" = $modifiedIterationPath
        "System.Reason" = GetFieldOrDefault -fields $fields -fieldName "System.Reason"
        "System.State" = "New"
        "System.State-real" = $realState
        "System.Description" = GetFieldOrDefault -fields $fields -fieldName "System.Description"
    }

    switch ($workItemType) {
        'Epic' {
            $baseData['Microsoft.VSTS.Common.AcceptanceCriteria'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.AcceptanceCriteria'
            $baseData['Microsoft.VSTS.Scheduling.StartDate'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Scheduling.StartDate'
            $baseData['Microsoft.VSTS.Scheduling.TargetDate'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Scheduling.TargetDate'
            $baseData['Microsoft.VSTS.Common.Priority'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.Priority' -dataType 'integer'
        }
        'Feature' {
            $baseData['Microsoft.VSTS.Common.AcceptanceCriteria'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.AcceptanceCriteria'
            $baseData['Microsoft.VSTS.Scheduling.StartDate'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Scheduling.StartDate'
            $baseData['Microsoft.VSTS.Scheduling.TargetDate'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Scheduling.TargetDate'
            $baseData['Microsoft.VSTS.Common.Priority'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.Priority' -dataType 'integer'
            $baseData['Microsoft.VSTS.Common.TimeCriticality'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.TimeCriticality'
        }
        'PBI' {
            $baseData['Microsoft.VSTS.Common.AcceptanceCriteria'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.AcceptanceCriteria'
            $baseData['Microsoft.VSTS.Common.Activity'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.Activity'
            $baseData['Microsoft.VSTS.Scheduling.DueDate'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Scheduling.DueDate'
            $baseData['Microsoft.VSTS.Common.Priority'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.Priority' -dataType 'integer'
        }
        'Task' {
            $baseData['Microsoft.VSTS.Common.Activity'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.Activity'
            $baseData['Microsoft.VSTS.Scheduling.DueDate'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Scheduling.DueDate'
            $baseData['Microsoft.VSTS.Scheduling.OriginalEstimate'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Scheduling.OriginalEstimate'
            $baseData['Microsoft.VSTS.Common.Priority'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.Priority' -dataType 'integer'
        }
        'Bug' {
            $baseData['Microsoft.VSTS.Common.AcceptanceCriteria'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.AcceptanceCriteria'
            $baseData['Microsoft.VSTS.Scheduling.StartDate'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Scheduling.StartDate'
            $baseData['Microsoft.VSTS.Scheduling.TargetDate'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Scheduling.TargetDate'
            $baseData['Microsoft.VSTS.Common.Priority'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.Priority' -dataType 'integer'
            $baseData['Microsoft.VSTS.Common.TimeCriticality'] = GetFieldOrDefault -fields $fields -fieldName 'Microsoft.VSTS.Common.TimeCriticality'
        }
    }
    return $baseData
}

# Recursive function to display and create JSON based on hierarchy
function DisplayAndCreateJsonForHierarchy {
    param([string]$url, [int]$level)

    try {
        $wiResponse = GetWorkItemDetails -url $url
        if ($wiResponse -eq $null) { return }

        $workItemType = if ($wiResponse.fields.'System.WorkItemType' -eq "Initiative") { "Epic" } else { $wiResponse.fields.'System.WorkItemType' }
        $indent = ("`t" * $level)
        Write-Host "${indent}Work Item ID:   $($wiResponse.id)"
        Write-Host "${indent}Title:          $($wiResponse.fields.'System.Title')"
        Write-Host "${indent}Type:           $workItemType"
        Write-Host "${indent}Area Path:      $($wiResponse.fields.'System.AreaPath')"
        Write-Host "${indent}------------------------------------------------------------------------------------------------------------"

        # Generate data object for JSON
        $dataObject = GenerateDataObject -detail $wiResponse -type $workItemType

        # Sanitize the specified area path to replace backslashes with hyphens
        $sanitizedAreaPath = $specifiedAreaPath -replace '\\', '-'

        # Define the file path using the sanitized area path
        $filePath = "C:\AdoProjects\log\$sanitizedAreaPath\payload-$($dataObject.'old-id')-$($dataObject.'System.WorkItemType').json"

        # Check if the directory exists; if not, create it
        $directoryPath = [System.IO.Path]::GetDirectoryName($filePath)
        if (!(Test-Path -Path $directoryPath)) {
            New-Item -ItemType Directory -Path $directoryPath | Out-Null
        }

        # Create the JSON file at the specified path
        CreateJsonFile -details $dataObject -filePath $filePath
        Write-Host "${indent}Created JSON file for $($dataObject.'old-id')"

        # Check for parent relation and process recursively
        $parentFound = $false
        foreach ($relation in $wiResponse.relations) {
            if ($relation.attributes.name -eq "Parent") {
                $parentFound = $true
                DisplayAndCreateJsonForHierarchy -url $relation.url -level ($level + 1)
            }
        }

        if (-not $parentFound) {
            Write-Host "${indent}`tNo further parent - reached the top level."
            Write-Host "${indent}------------------------------------------------------------------------------------------------------------"
        }
    }
    catch {
        Write-Host "Error retrieving parent work item details: " $_.Exception.Message
    }
}

# Main script execution
$srcOrganization = "http://moht-tfsado01:8080/tfs/"
$srcCollection = "DefaultCollection/"
$srcProject = "BI_EDW_SCRUM/"
$specifiedAreaPath = "BI_EDW_SCRUM\תחום דאטה\BI-GIS\צוות 3"
$logFile = ("{0}{1}.{2}" -f "C:\AdoProjects\log\loginit_", (Get-Date).ToString("yyyyMMddHHmmss"), "txt")

Start-Transcript -Path $logFile -Append
$startTime = Get-Date

# Fetch and process work items
try {
    $workItems = GetWorkItemsByAreaPath -organization $srcOrganization -collection $srcCollection -project $srcProject -areaPath $specifiedAreaPath
    if ($workItems -ne $null -and $workItems.Count -gt 0) {
        Write-Host "Total Work Items found: $($workItems.Count)"
        $counter = 0
        foreach ($item in $workItems) {
            Write-Host "Processing Work Item: $counter"
            DisplayAndCreateJsonForHierarchy -url $item.url -level 0
            $counter += 1
        }

        Write-Host "**************************************************************************"
        Write-Host "Total Work Items Processed: $counter"
        Write-Host "**************************************************************************"
    } else {
        Write-Host "No work items found in the specified area path."
    }
} catch {
    Write-Host "An error occurred: " $_.Exception.Message
}

$endTime = Get-Date
Write-Host "Duration: " ($endTime - $startTime).ToString()

Stop-Transcript