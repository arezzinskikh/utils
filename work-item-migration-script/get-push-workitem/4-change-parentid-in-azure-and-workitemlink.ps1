# Set up to ignore SSL warnings - not recommended for production
Add-Type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@

# Configure security protocols
$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

# Connection parameters
$OrganizationUrl = "http://moht-tfsado01:8080/tfs"
$CollectionName = "MohCollection"
$ProjectName = "MohMain"
$Token = "a5kclriyh7l74b3xddix5rcg5phdzudo3oylfcsyffblu4fu56kq"
$apiVersion = "6.0"

# Authentication Header
$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(":$Token"))

# Read JSON file with work item links
$jsonFilePath = "C:\AdoProjects\log\workItemLinks.json"
$workItems = Get-Content $jsonFilePath -Raw | ConvertFrom-Json

# Creating a lookup table for old ID to new ReferenceID
$idLookup = @{}
foreach ($item in $workItems) {
    foreach ($key in $item.PSObject.Properties.Name) {
        $oldId = $key
        $newId = $item.$key.ReferenceID
        $idLookup[$oldId] = $newId
    }
}

# Function to link a child work item to a parent using the new ReferenceID
function Link-WorkItems($parentId, $childId) {
    $newParentId = $idLookup["$parentId"]  # Retrieve new ReferenceID using old ID from the lookup
    if ($null -eq $newParentId) {
        Write-Host "No parent ID for work item $childId. Skipping link creation."
        return
    }
    $uri = "$($OrganizationUrl)/$($CollectionName)/$($ProjectName)/_apis/wit/workitems/$($childId)?api-version=$apiVersion"
    $body = @"
[
    {
        "op": "add",
        "path": "/relations/-",
        "value": {
            "rel": "System.LinkTypes.Hierarchy-Reverse",
            "url": "$($OrganizationUrl)/$($CollectionName)/_apis/wit/workItems/$($newParentId)",
            "attributes": {
                "comment": "Linking parent work item using new ReferenceID"
            }
        }
    }
]
"@

    Try {
        $result = Invoke-RestMethod -Uri $uri -Method Patch -Headers @{Authorization=("Basic {0}" -f $base64AuthInfo)} -ContentType "application/json-patch+json" -Body $body
        Write-Host "The work item $childId is successfully linked to new parent work item $newParentId."
    } Catch {
        Write-Host "Failed to link work item $childId to new parent $newParentId."
        Write-Host "HTTP Error: $($_.Exception.Response.StatusCode.Value__) - $($_.Exception.Response.StatusDescription)"
    }
}

# Iterate through the work items and link them using the new ReferenceID if applicable
foreach ($item in $workItems) {
    foreach ($key in $item.PSObject.Properties.Name) {
        $childId = $item.$key.ReferenceID
        $parentId = $item.$key.ParentReferenceID
        if ($parentId -ne $null) {
            Link-WorkItems -parentId $parentId -childId $childId
        } else {
            Write-Host "No parent ID for work item $childId. Skipping link creation."
        }
    }
}