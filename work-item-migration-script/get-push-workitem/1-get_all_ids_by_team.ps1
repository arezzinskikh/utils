# Trust all certificates
add-type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@
$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

# Function to get all work items based on area path
function GetAllEpicsForTeam {
    param([string]$organization,[string]$srcCollection,[string]$srcProject)

    try {
        $getWiUrl=("{0}{1}{2}{3}" -f $organization,$srcCollection,$srcProject,"_apis/wit/wiql?api-version=5.0")
        
        # Query to retrieve all work items in the specified Area Path
        $body=@{
            "query"= "Select [System.Id],[System.Title],[System.State] From WorkItems Where [Area Path] IN ('BI_EDW_SCRUM\תחום דאטה\BI-GIS\צוות אנליזה\אנליזה צוות 1 BI')"
        }

        $wiResponse=Invoke-RestMethod -Uri $getWiUrl -Method POST -Body (ConvertTo-Json -InputObject $body) -ContentType "application/json;charset=UTF-8" -UseDefaultCredentials
        write-host "Total Work Items found: "$wiResponse.workItems.Count
        return $wiResponse
    }
    catch {
        $_.Exception.Response
        return $null
    }
}

# Function to get a work item by its URL
function GetWorkItemByUrl {
    param([string]$url)

    try {
        $wiUrl=("{0}{1}" -f $url,"?`$`expand=all")
        $wiResponse=Invoke-RestMethod -Uri $wiUrl -Method Get -ContentType "application/json" -UseDefaultCredentials
        return $wiResponse
    }
    catch {
        $_.Exception.Response
        return $null
    }
}

# Recursive function to retrieve parent items up to the highest level
function GetParentWorkItems {
    param([string]$url, [int]$level)

    try {
        $wiResponse = GetWorkItemByUrl -url $url
        if ($wiResponse -eq $null) { return }

        # Display work item details with indentation
        $id = $wiResponse.fields.'System.Id'
        $title = $wiResponse.fields.'System.Title'
        $type = $wiResponse.fields.'System.WorkItemType'
        $areaPath = $wiResponse.fields.'System.AreaPath'

        write-host ("`t" * $level) "Work Item ID:   $id"
        write-host ("`t" * $level) "Title:          $title"
        write-host ("`t" * $level) "Type:           $type"
        write-host ("`t" * $level) "Area Path:      $areaPath"
        write-host ("`t" * $level) "------------------------------------------------------------------------------------------------------------"

        # Check for parent relation
        $parentFound = $false
        foreach ($relation in $wiResponse.relations) {
            if ($relation.attributes.name -eq "Parent") {
                $parentFound = $true
                # Recursive call for the parent work item
                GetParentWorkItems -url $relation.url -level ($level + 1)
            }
        }

        if (-not $parentFound) {
            write-host ("`t" * ($level + 1)) "No further parent - reached the top level."
            write-host ("`t" * $level) "------------------------------------------------------------------------------------------------------------"
        }
    }
    catch {
        $_.Exception.Response
        return
    }
}

# Main script to execute the hierarchy retrieval
$srcOrganization = "http://moht-tfsado01:8080/tfs/"
$srcCollection = "DefaultCollection/"
$srcProject = "BI_EDW_SCRUM/"

$logFile = ("{0}{1}.{2}" -f "C:\AdoProjects\log\loginit_", (Get-Date).ToString("yyyyMMddHHmmss"), "txt")
Start-Transcript -path $logFile -append
$startTime = (Get-Date)

try {
    $allWiResponse = GetAllEpicsForTeam -organization $srcOrganization -srcCollection $srcCollection -srcProject $srcProject
    $counter = 0

    foreach ($item in $allWiResponse.workItems) {
        write-host "Processing Work Item: $counter"
        $counter += 1

        # Start from each child and go up the hierarchy
        GetParentWorkItems -url $item.url -level 0
    }

    write-host "**************************************************************************"
    write-host "Total Work Items Processed: " $allWiResponse.workItems.Count
    write-host "**************************************************************************"
}
catch {
    $_.Exception.Response
    return
}

$endTime = (Get-Date)
'Duration: {0:mm} min {0:ss} sec' -f ($endTime - $startTime)
Stop-Transcript