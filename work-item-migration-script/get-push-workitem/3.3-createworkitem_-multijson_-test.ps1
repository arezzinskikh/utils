# Trust all certificates
add-type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@
$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

# Function to create a work item
function CreateWorkItem {
    param(
        [string]$organizationUrl,
        [string]$collectionName,
        [string]$projectName,
        [string]$areaPath,
        [PSCustomObject]$workItemDetails,
        [string]$jsonFileName
    )

    $WorkItemType = $workItemDetails.'System.WorkItemType'
    if (-not $WorkItemType) {
        Write-Host "Error: WorkItemType is missing in the JSON data. Skipping."
        return $null
    }

    $apiVersion = "6.0"
    $createWiUrl = "$organizationUrl/$collectionName/$projectName/_apis/wit/workitems/`$$($WorkItemType)?api-version=$apiVersion"
    $body = @()

    foreach ($key in $workItemDetails.PSObject.Properties.Name) {
        $value = $workItemDetails.$key
        if ($key -ne 'ReferenceID' -and $key -ne 'ParentReferenceID' -and $key -ne 'old-id' -and $key -ne 'System.Reason' -and $key -ne 'System.AssignedTo' -and $key -ne 'System.IterationPath' -and $key -ne 'System.State-real') {
            $body += @{ op = "add"; path = "/fields/$key"; value = $value }
        }
    }
    
    if ($body.Count -eq 0) {
        Write-Host "Error: No fields were added to the JSON body. Skipping this work item."
        return $null
    }

    $jsonBody = $body | ConvertTo-Json -Depth 10
    $utf8 = New-Object System.Text.UTF8Encoding
    $encodedBody = $utf8.GetBytes($jsonBody)

    try {
        $response = Invoke-RestMethod -Uri $createWiUrl -Method Post -ContentType "application/json-patch+json; charset=utf-8" -Body $encodedBody -UseDefaultCredentials
        $message = "Work item created successfully. ID: $($response.id) $($WorkItemType) from $jsonFileName"
        Write-Host $message
        Add-Content -Path $logPath -Value $message
        return $response.id
    } catch {
        Write-Host "Failed to create work item."
        if ($_.Exception.Response) {
            $reader = New-Object System.IO.StreamReader($_.Exception.Response.GetResponseStream())
            $responseBody = $reader.ReadToEnd()
            Write-Host "Response Body: $responseBody"
            $reader.Close()
        } else {
            Write-Host "No response body available."
        }
        return $null
    }
}

$organizationUrl = "http://moht-tfsado01:8080/tfs"
$collectionName = "MohCollection"
$projectName = "MohMain"
$areaPath = "DATA-management\Migration-test"
$specifiedAreaPath = "BI_EDW_SCRUM\תחום דאטה\BI-GIS\צוות 3"
$sanitizedAreaPath = $specifiedAreaPath -replace '\\', '-'
$logPath = "C:\AdoProjects\log\create_new_workitem_mohmain_" + (Get-Date).ToString("yyyyMMddHHmmss") + ".txt"
$newIdLogPath = "C:\AdoProjects\log\change_to_newID_workitem_mohmain_" + (Get-Date).ToString("yyyyMMddHHmmss") + ".txt"

# Ensure the directory for sanitized area path exists
$areaDirectoryPath = "C:\AdoProjects\log\$sanitizedAreaPath"
if (!(Test-Path -Path $areaDirectoryPath)) {
    New-Item -ItemType Directory -Path $areaDirectoryPath | Out-Null
}

# Processing JSON files to create work items
Get-ChildItem -Path $areaDirectoryPath -Filter "payload*.json" | ForEach-Object {
    $jsonFilePath = $_.FullName
    $workItemDetails = Get-Content -Path $jsonFilePath | ConvertFrom-Json
    if ($workItemDetails) {
        $newId = CreateWorkItem -organizationUrl $organizationUrl -collectionName $collectionName -projectName $projectName -areaPath $areaPath -workItemDetails $workItemDetails -jsonFileName $_.Name
        if ($newId) {
            $workItemDetails.ReferenceID = $newId
            $workItemDetails | ConvertTo-Json -Depth 10 | Set-Content -Path $jsonFilePath
            $changeMessage = "Updated ReferenceID in `$jsonFileName` to $newId"
            Write-Host $changeMessage
            Add-Content -Path $newIdLogPath -Value $changeMessage
        }
    } else {
        Write-Host "Warning: Skipping empty or invalid JSON file: $jsonFilePath"
    }
}

# Function for creating reference mapping JSON file
function CreateReferenceMapping {
    param(
        [string]$inputPath,
        [string]$outputPath = "C:\AdoProjects\log",
        [string]$outputFileName = "workitemlinks.json"
    )

    $finalOutputPath = Join-Path -Path $outputPath -ChildPath $outputFileName

    # Load existing data if the file exists
    $mapping = @{}
    if (Test-Path -Path $finalOutputPath) {
        try {
            $existingContent = Get-Content -Path $finalOutputPath -Raw | ConvertFrom-Json
            foreach ($item in $existingContent.PSObject.Properties) {
                $mapping[$item.Name] = $item.Value
            }
            Write-Host "Loaded existing data with $($mapping.Count) entries."
        } catch {
            Write-Host "Failed to load or parse existing JSON data. Initializing new data structure."
        }
    }

    # Process each JSON file in the input path
    Get-ChildItem -Path $inputPath -Filter "payload*.json" | ForEach-Object {
        $jsonFilePath = $_.FullName
        $workItemDetails = Get-Content -Path $jsonFilePath | ConvertFrom-Json

        $oldIdStr = [string]$workItemDetails.'old-id'
        if ($mapping.ContainsKey($oldIdStr)) {
            Write-Host "Entry for old-id $oldIdStr already exists. Skipping update."
        } else {
            $referenceId = $workItemDetails.'ReferenceID'
            $hyperlink = "$organizationUrl/$collectionName/$projectName/_workitems/edit/$referenceId"
            Write-Host "Adding new entry for old-id $oldIdStr."
            $mapping[$oldIdStr] = @{
                "ReferenceID" = $referenceId
                "ParentReferenceID" = $workItemDetails.'ParentReferenceID'
                "Title" = $workItemDetails.'System.Title'
                "WorkItemType" = $workItemDetails.'System.WorkItemType'
                "StateReal" = $workItemDetails.'System.State-real'
                "IterationPath" = ($workItemDetails.'System.IterationPath' -replace '\\', '\\')
                "AssignedTo" = $workItemDetails.'System.AssignedTo'
                "Reason" = $workItemDetails.'System.Reason'
                "hyperlink" = $hyperlink
            }
        }
    }

    # Save the updated mapping to JSON
    $jsonObject = $mapping | ConvertTo-Json -Depth 10
    $jsonObject | Set-Content -Path $finalOutputPath
    Write-Host "Mapping JSON file created or updated at: $finalOutputPath"
}


# You might need to call this function like this:
$fullInputPath = Join-Path -Path "C:\AdoProjects\log" -ChildPath $sanitizedAreaPath

# Call to the function using the dynamic path
CreateReferenceMapping -inputPath $fullInputPath