variable "cluster_name" {
  description = "Name of the ECS cluster to deploy tasks"
  type        = string
}

variable "task_family" {
  description = "Family of the ECS task definition"
  type        = string
}

variable "image_tag" {
  description = "Tag of the Docker image in the ECR repository"
  type        = string
}

variable "container_name" {
  description = "Name for the container in the task"
  type        = string
}

variable "container_port" {
  description = "Port on which the container will accept connections"
  type        = number
}

variable "task_cpu" {
  type        = string
  description = "The number of CPU units used by the task"
}

variable "task_memory" {
  type        = string
  description = "The amount of memory used by the task (in MiB)"
}