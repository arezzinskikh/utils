cluster_name    = "demo4"
task_family     = "demo-guy"
image_tag       = "flask-app"
container_name  = "hello-world-pyton-demo"
container_port  = 80
task_cpu        = "256"
task_memory     = "512"
