resource "aws_ecs_task_definition" "task" {
  family                   = var.task_family
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.task_cpu
  memory                   = var.task_memory
  execution_role_arn       = "arn:aws:iam::244271652366:role/unique_ecs_task_execution_role"

  container_definitions = jsonencode([{
    name  = var.container_name,
    image = "${data.aws_ecr_repository.ecr_repo.repository_url}:${var.image_tag}",
    portMappings = [{
      containerPort = var.container_port,
      hostPort      = var.container_port
    }],
  }])
}

