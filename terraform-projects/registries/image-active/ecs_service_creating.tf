resource "aws_ecs_service" "my_service" {
  name            = "testing"
  task_definition = aws_ecs_task_definition.task.arn
  launch_type     = "FARGATE"

  desired_count   = 1  

  network_configuration {
    subnets = ["subnet-04cb5170495d3cf7b", "subnet-0a3c12182870a4d29"]  
    security_groups = ["sg-0924509b28ec908ee"]  
    assign_public_ip = true  
  }

  // load_balancer {
  //   target_group_arn = "arn:aws:elasticloadbalancing:region:account-id:targetgroup/terget-group/1234567890123456"
  //   container_name   = var.container_name
  //   container_port   = var.container_port
  // }

}