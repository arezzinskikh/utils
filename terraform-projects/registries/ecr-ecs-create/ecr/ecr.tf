resource "aws_ecr_repository" "registry" {
   for_each             = toset(var.repository_name)
   name                 = each.value
  image_tag_mutability = "MUTABLE"
}


