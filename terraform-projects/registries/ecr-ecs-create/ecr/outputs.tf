output "ecr_repository_urls" {
  value = { for idx, repo in aws_ecr_repository.registry : idx => repo.repository_url }
}

output "repository_names" {
  value = var.repository_name
}

output "debug_repository_name" {
  value = var.repository_name
}