module "ecr" {
  source          = "./ecr"
  repository_name = var.repository_name
}

module "ecs" {
  source        = "./ecs"
  cluster_name  = var.cluster_name
}
