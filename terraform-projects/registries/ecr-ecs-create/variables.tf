variable "repository_name" {
  type        = list(string)
  description = "Name of the ECR repository"
}

variable "cluster_name" {
  type        = string
  description = "The name of the ECS cluster"
}
